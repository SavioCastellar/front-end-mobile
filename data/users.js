export default [
    {
        id: 1,
        name: "Sávio Castellar",
        email: "saviocastellar@gmail.com",
        avatarUrl: "https://pbs.twimg.com/media/EkzV-C1XIAYGmGf?format=jpg&name=small"
    },
    {
        id: 2,
        name: "Vítor Castellar",
        email: "vitorrcastellar@gmail.com",
        avatarUrl: "https://i.pinimg.com/564x/81/24/fd/8124fddb29a30e83f4ee46666efd9c6e.jpg"
    }
];