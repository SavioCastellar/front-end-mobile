import { getActionFromState } from '@react-navigation/native';
import { ListItem } from '@rneui/base';
import React, {Component, useContext} from 'react';
import {View, Text, FlatList} from "react-native";
import UsersContext from '../context/UsersContext';

export default props => {

    const {state, dispatch} = useContext(UsersContext)
    console.warn(Object.keys(ctx.state))

    function confirmUserDelete(user){
        Alert.alert("Excluir Usuário", "Deseja excluir o usuário?", [
            {
                text: "Sim",
                onPress() {
                    dispatch({
                        type: "deleteUser",
                        payload: user,
                        
                    })
                }
            },
            {
                text: "Não"
            }
        ])
    }

    function getActions(user) {
        return (
            <>
                <Button
                    onPress={() => props.navigation.navigate("UserForm", user)}
                    type="clear"
                    icon={<Icon name="edit" size={25} color="orange" />}
                />
                <Button
                    onPress={() => confirmUserDelete(user)}
                    type="clear"
                    icon={<Icon name="delete" size={25} color="red" />}
                />
            </>
        )
    }

    function getUserItem({ item: user }) {
        return (
            <ListItem
                leftAvatar={{source: {uri: user.avatarUrl}}}
                key={user.id}
                title={user.name}
                subtitle={user.email}
                bottomDivider
                rightElement={getActions(user)}
                onpress={() => props.navigation.navigate("UserForm", user)}
            />
        )
    }

    return (
        <View>
            <FlatList
                keyExtractor={users => users.id.toString()}
                data={state.users}
                renderItem={getUserItem}
            />
        </View>
    )
}